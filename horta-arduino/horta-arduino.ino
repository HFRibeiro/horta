#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <WiFiClient.h>
#include <Wire.h>
#include <SHT1x.h>

#define dataPin 0
#define clockPin 2
SHT1x sht1x(dataPin, clockPin);

ADC_MODE(ADC_VCC);

#define DEEPSLEEP_INTERVAL 300  //in sec

//Your Domain name with URL path or IP address with path
String serverName = "http://192.168.1.11:80/";

void setup() {
  Serial.begin(115200);

  Serial.println("Configuring Wifi...");
  WiFi.persistent(false);
  WiFi.mode(WIFI_STA);
  //WiFi.config(ip, gateway, subnet);
  WiFi.begin("MEO", "11dbmtpi");

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  Serial.println("");
  Serial.println("Device started");

  Serial.print("Battery voltage: ");
  float Vbat = ESP.getVcc() / 1000.0f;
  Serial.print(Vbat);
  Serial.println(" V");

  float temperature = sht1x.readTemperatureC();
  float humidity = sht1x.readHumidity();

  if (WiFi.status() == WL_CONNECTED) {
    WiFiClient client;
    HTTPClient http;

    String serverPath = serverName + "horta.php?bat=" + String(Vbat) + "&temp=" + String(temperature) + "&hum=" + String(humidity);

    // Your Domain name with URL path or IP address with path
    http.begin(client, serverPath.c_str());

    // Send HTTP GET request
    int httpResponseCode = http.GET();

    if (httpResponseCode > 0) {
      Serial.print("HTTP Response code: ");
      Serial.println(httpResponseCode);
      String payload = http.getString();
      Serial.println(payload);
    } else {
      Serial.print("Error code: ");
      Serial.println(httpResponseCode);
    }
    // Free resources
    http.end();
  } else {
    Serial.println("WiFi Disconnected");
  }

  delay(300);
  Serial.println("I'm awake, but I'm going into deep sleep mode for 30 seconds");
  ESP.deepSleep(DEEPSLEEP_INTERVAL * 1000000);
}

void loop() {}


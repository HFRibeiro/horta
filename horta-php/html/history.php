<?php

header('Access-Control-Allow-Origin: *'); 
date_default_timezone_set("Europe/Lisbon");
header('charset=utf-8'); 

$servername = "localhost";
$username = "root";
$password = "lar";
$dbname = "horta";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

$conn->set_charset("utf8mb4");
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT * FROM `data` ORDER BY `data`.`id` DESC LIMIT 1000";
$result = $conn->query($sql);

$dataArray = [];
if ($result->num_rows > 0) {
while ($row = $result->fetch_assoc()) {
  $json = $row['data'];
  $dataObj = json_decode($json, true);
  $dataObj['timestamp'] = $row['timestamp'];
  $updatedJson = json_encode($dataObj);
  array_push($dataArray,$updatedJson);
}
} else echo '{"error": "0 results"}';
echo json_encode($dataArray);
?>
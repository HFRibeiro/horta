<?php

header('Access-Control-Allow-Origin: *'); 
date_default_timezone_set("Europe/Lisbon");
header('charset=utf-8'); 

$servername = "localhost";
$username = "root";
$password = "lar";
$dbname = "horta";

$count = $_GET['count'];
$text = $_GET['text'];

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

$conn->set_charset("utf8mb4");
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}


$sql = "SELECT * FROM `data` ORDER BY `data`.`id` DESC LIMIT 1";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
  $row = $result->fetch_assoc();
  $json = $row['data'];
  //$json['timestamp'] = $row['timestamp'];
  $dataArray = json_decode($json, true);

  // Add the new key-value pair (timestamp)
  $dataArray['timestamp'] = $row['timestamp']; // or use any other value you'd like to set as the timestamp

  // Encode the updated associative array back to a JSON string
  $updatedJson = json_encode($dataArray);

  // Print the updated JSON string
  print_r($updatedJson);
} else echo '{"error": "0 results"}';

//echo '{ "temperature": '.rand(0,80).', "humidity": '.rand(0,100).' }';
?>
import './App.css';
import TemperatureHumidity from './components/TemperatureHumidity';
import Graph from './components/Graph';

const App = () => {
  
  return (
    <div className="App">
      <header className="App-header">
        <TemperatureHumidity />
        <Graph/>
      </header>
    </div>
  );
}

export default App;

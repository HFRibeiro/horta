import React from 'react';
import { RiCloseLine } from 'react-icons/ri';
import './CloseButton.css';

function CloseButton(props) {
  const handleClick = () => {
    if (props.onClick) {
      props.onClick();
    }
  };

  return (
    <div className="close-button" onClick={handleClick} title='close'>
      <RiCloseLine color='white' />
    </div>
  );
}

export default CloseButton;
import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { useSelector } from 'react-redux';
import Plot from 'react-plotly.js';
import { useDispatch } from 'react-redux';
import './Graph.css';
import CloseButton from './CloseButton/CloseButton';
import { CHANGE_STATE } from '../store/actions';

const maxBat = 3.7;

const Graph = () => {

    const [data, setData] = useState([]);
    const display = useSelector((state) => state.display);
    const dispatch = useDispatch();

    useEffect(() => {
        const fetchData = async () => {
            try {
                const response = await axios.get('/history.php');
                const data = response.data;
                setData(data);
            } catch (error) {
                console.error('Error fetching data:', error);
            }
        };

        if (display !== 0) fetchData()
    }, [display]);

    const x = [];
    const y = [];
    data.forEach(element => {
        const data = JSON.parse(element);
        x.push(data.timestamp);
        if (display === 1) y.push(data.temp);
        else if (display === 2) y.push(data.hum);
        else if (display === 3) {
            const batPercentage = parseFloat(data.bat / maxBat * 100).toFixed(2);
            y.push(batPercentage);
        }
    });

    const configuration = {
        modeBarButtonsToRemove: ['zoom', 'pan2d', 'select2d', 'lasso2d', 'resetScale2d'],
        displaylogo: false,
        displayModeBar: 'hover',
        toImageButtonOptions: {
            filename: 'LineGraph_' + new Date().toLocaleString('pt-pt'),
            width: 800,
            height: 600,
            format: 'jpeg'
        }
    }

    return (
        display !== 0 ?
            <div className='plot-div'>
                <CloseButton onClick={() => { dispatch({ type: CHANGE_STATE, payload: { display: 0 } }) }} />
                <Plot
                    data={[
                        {
                            x,
                            y,
                            type: 'scatter',
                            mode: 'lines',
                            marker: { color: 'lightblue' },
                        },
                    ]}
                    layout={{
                        title: display === 1 ? 'Temperatura' : display === 2 ? 'Humidade' : 'Bateria',
                        datarevision: 1,
                        autosize: true,
                        margin: {
                            l: 20,
                            r: 0,
                            b: 30,
                            t: 30,
                            pad: 1
                        },
                        xaxis: {
                            gridcolor: 'rgba(255,255,255,0.2)',
                        },
                        yaxis: {
                            gridcolor: 'rgba(255,255,255,0.2)',
                        },
                        plot_bgcolor: "rgba(0,0,0,0)",
                        paper_bgcolor: "rgba(0,0,0,0)",
                        font: {
                            color: 'rgba(255,255,255,0.6)'
                        },
                        modebar: {
                            activecolor: 'white',
                            bgcolor: 'transparent'
                        },
                        dragmode: 'zoom'
                    }}
                    config={configuration}
                    graphDiv="graph"
                    className='plot-class'
                    useResizeHandler={true}

                />
            </div> : null
    );
}

export default Graph;

import React, { useState, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import axios from 'axios';
import { CHANGE_STATE } from '../store/actions';
import { CircularProgressbar, buildStyles } from 'react-circular-progressbar';
import ProgressBar from 'react-bootstrap/ProgressBar';
import 'react-circular-progressbar/dist/styles.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import './TemperatureHumidity.css';

const maxBat = 3.7;

function getColor(temp) {
  if (temp >= -20 && temp < 20) {
    return 'lightblue';
  } else if (temp >= 20 && temp < 30) {
    return 'palegoldenrod';
  } else {
    return 'orange';
  }
}

const TemperatureHumidity = () => {
  const [data, setData] = useState({ bat: 0, temp: 0, hum: 0, timestamp: 0 });
  const batPercentage = parseFloat(data.bat / maxBat * 100).toFixed(2);

  const dispatch = useDispatch();

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get('/temperature_humidity.php');
        const data = response.data;
        setData(data);
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    };

    fetchData(); // Fetch data immediately on component mount

    const intervalId = setInterval(() => {
      fetchData(); // Fetch data every 30 seconds
    }, 30000);

    return () => {
      clearInterval(intervalId); // Clear the interval when the component unmounts
    };
  }, []);


  return (
    <div className={"container"}>
      <div className={"progress-bar"}>
        <CircularProgressbar
          value={data.temp}
          maxValue={80}
          text={<tspan className='text-label' onClick={() => {
            dispatch({type: CHANGE_STATE, payload: {display: 1}});
          }}>{data.temp + "ºc"}</tspan>}
          styles={buildStyles({
            textColor: getColor(data.temp),
            pathColor: getColor(data.temp),
            trailColor: '#d6d6d6',
          })}
        />
      </div>
      <div className={"progress-bar"}>
        <CircularProgressbar
          value={data.hum}
          maxValue={100}
          text={<tspan className='text-label' onClick={() => {
            dispatch({type: CHANGE_STATE, payload: {display: 2}});
          }}>{data.hum + "%"}</tspan>}
        />
      </div>
      <div className={"progress-margin"} >
        <ProgressBar
          striped
          now={batPercentage}
          variant={batPercentage > 75 ? "success" : batPercentage > 50 ? "warning" : "danger"}
        />
        <span onClick={() => {
            dispatch({type: CHANGE_STATE, payload: {display: 3}});
          }} className='bat-percentage'>{batPercentage}%</span><br></br>
        <span style={{ fontSize: '0.8em' }}>{data.timestamp}</span>
      </div>
    </div>
  );
};

export default TemperatureHumidity;

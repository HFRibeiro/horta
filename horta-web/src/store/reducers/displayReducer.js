import { CHANGE_STATE } from '../actions';

export const displayReducer = (state = 0, action) => {
    switch (action.type) {
        case CHANGE_STATE:
            const { display } = action.payload;
            return display;
        default:
            return state;
    }
};